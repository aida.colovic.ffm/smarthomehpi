/*******************************************************************************************
 * Aida Čolović
 * Smart Home Projcet
 ******************************************************************************************/

/************************************* Libraries ******************************************/

#include <WiFi.h>
#include <WiFiClient.h>
#include <WiFiAP.h>

#include "Adafruit_MQTT.h"
#include "Adafruit_MQTT_Client.h"

/********************************** Global variables **************************************/

#define RELAY_1   12
#define BUTTON    36

/********************************** Adafruit.io Setup *************************************/

#define MQTT_SERV "io.adafruit.com"
#define MQTT_PORT 1883
#define MQTT_NAME "******"                              //Adafruit username
#define MQTT_PASS "********************************"    //Adafruit IO key

/************************************ WiFi AP *********************************************/

#define WIFI_SSID "********"                            //your WiFi SSID
#define WIFI_PASS "************"                        //your WiFi password

/******************* Global State (you don't need to change this!) ************************/

// Create an WiFiClient class to connect to the MQTT server.
WiFiClient client;

// Setup the MQTT client class by passing in the WiFi client and MQTT server and login details.
Adafruit_MQTT_Client mqtt(&client, MQTT_SERV, MQTT_PORT, MQTT_NAME, MQTT_PASS);

/************************************* Feeds **********************************************/

Adafruit_MQTT_Subscribe onOff       = Adafruit_MQTT_Subscribe(&mqtt, MQTT_NAME "/f/onOff"); 
Adafruit_MQTT_Publish   onOffStatus = Adafruit_MQTT_Publish(&mqtt, MQTT_NAME "/f/status"); 

/********************************** Decleration *******************************************/

void buttonIsr();
void MQTT_connect();


//#################################### Sketch ############################################\\


/************************************* setup() *********************************************
 * Setup Loop
 ******************************************************************************************/
void setup() 
{ 
  Serial.begin(115200);
   
  pinMode(BUTTON, INPUT);                   
  pinMode(RELAY_1, OUTPUT);
  pinMode(LED_BUILTIN, OUTPUT);

  attachInterrupt(BUTTON,buttonIsr,RISING);
  digitalWrite(RELAY_1, HIGH);

  // Connect to WiFi AP.
  Serial.println(); Serial.println();
  Serial.print("Connecting to ");
  Serial.println(WIFI_SSID);
  
  WiFi.begin(WIFI_SSID, WIFI_PASS);
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(">");
  }
  Serial.println();

  Serial.println("WiFi connected");
  Serial.println("IP address: "); Serial.println(WiFi.localIP());

  // Setup MQTT subscription for onoff feed.
  mqtt.subscribe(&onOff);
}

/*************************************** loop() *********************************************
 * Main Loop
 *******************************************************************************************/
void loop() 
{
  Adafruit_MQTT_Subscribe * subscription;
  static bool lastPublishedValue = true;
  bool newValue;
  
  // Ensure the connection to the MQTT server is alive (this will make the first
  // connection and automatically reconnect when disconnected)./verbinden zum MQTT
  MQTT_connect();

  // ping the server to keep the mqtt connection alive
  // NOT required if you are publishing once every KEEPALIVE seconds
  /*
  if(! mqtt.ping()) {
    mqtt.disconnect();
  }
  */

  // this is our 'wait for incoming subscription packets' busy subloop
  // try to spend your time here
  while ((subscription = mqtt.readSubscription(500)))
  {
    if (subscription == &onOff)
    {
      //Print the new value to the serial monitor
      Serial.print("Ventilator Status: ");
      Serial.println((char*) onOff.lastread);

      //is the value "ON" turn on the fan
      if (!strcmp((char*) onOff.lastread, "ON"))
      {
        digitalWrite(RELAY_1, LOW);
        digitalWrite(LED_BUILTIN, HIGH);
      }
      //is the value "OFF" turn off the fan
      else if (!strcmp((char*) onOff.lastread, "OFF"))
      {
        digitalWrite(RELAY_1, HIGH);
        digitalWrite(LED_BUILTIN, LOW);
      }
      else
      {
        onOffStatus.publish("ERROR");
      }
    }
  }

  // check if state changed and publish new state
  newValue = digitalRead(RELAY_1);
  if(lastPublishedValue != newValue)
  {
    lastPublishedValue = newValue;
    if(lastPublishedValue)
    {
      onOffStatus.publish("OFF");
      Serial.println("Relay-State OFF");
    }
    else
    {
      onOffStatus.publish("ON");
      Serial.println("Relay-State ON");
    }
  }
}

/************************************* buttonIsr() ******************************************
 * Interrupt-Service-Routine
 *******************************************************************************************/
void buttonIsr() 
{
  static unsigned long  currentTime = 0;
  static unsigned long  lastTime = 0;
  bool currentState;
  bool newState;
  
  // debounce switch press
  currentTime = millis();
  if( (currentTime - lastTime) > 100)
  {
    lastTime = currentTime;
    currentState = digitalRead(RELAY_1);
    newState = currentState ^ 1;
    digitalWrite(RELAY_1, newState);
    Serial.println("Toggle");
  }
}

/************************************ MQTT_connect() ****************************************
 * Function to connect and reconnect as necessary to the MQTT server. Should be called in 
 * the loop function and it will take care if connecting.
 *******************************************************************************************/
void MQTT_connect()
{
  int8_t ret;
  
  //Stop if already connected
  if (mqtt.connected())
  {
    return;
  }

  Serial.print("Connecting to MQTT... ");
  uint8_t retries = 3;
  while ((ret = mqtt.connect()) != 0) // connect will return 0 for connected
  {
    Serial.println(mqtt.connectErrorString(ret));
    Serial.println("Retrying MQTT connection in 5 seconds...");
    mqtt.disconnect();
    delay(5000);  // wait 5 seconds
    retries--;
    if (retries == 0)
    {
      ESP.restart();
    }
  }
  Serial.println("MQTT Connected!");
}
